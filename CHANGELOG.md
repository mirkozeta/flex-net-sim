# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.2] - 2025-02-28 

### ADDED  
- **Major documentation update:**  
  - Added **comprehensive user guides** through custom pages, significantly expanding the documentation.  
  - Improved **readability and structure** with better formatting, styling, and organization.  
- Integrated **Doxygen-Awesome** as a submodule for improved styling.  
- Added badges to `README` and documentation page.  
- Added a README in `docs/` to guide the documentation structure and future maintainers.  
- Added macros:  
  - `REQ_SLOTS_FIXED`: Returns `bitRate.getNumberOfSlots(0)`.  
  - `REQ_REACH_FIXED`: Returns `bitRate.getReach(0)`.  

### CHANGED  
- Updated Doxygen styling and formatting.  
- Restructured documentation folder organization.  
- Moved Doxygen build to `docs/build/`.  
- Modified `e001` example for better clarity and coherence.  
- Improved README clarity and updated links to docs.  
- Migrated documentation from GitLab Pages to Read the Docs.  
- Modified `.gitlab-ci.yml`, removed `pages` stage, and added `coverage_analysis`.  

### FIXED  
- Standardized semicolon usage in `ALLOC_SLOTS`, `ALLOC_SLOTS_SDM`, and `ALLOC_SLOTS_MB`.  
  - Updated tests and examples accordingly.  
- Fixed incorrect version reference (0.8.0 → 0.8.1) in CHANGELOG URLs.  

## [0.8.1] - 2025-02-10
### CHANGED
- Standardised terminology by replacing "BDM" with "MB" across multiple files for consistency with Multiband (MB) network literature.
- Minor changes to documentation adding MB.
- Modified MB example: simplified structure for clearer usage implementing an RMLBSA first-fit algorithm.
- Simplified and modularized JSON parsing in the `Network` class:
  - Common network initialization code is now in the constructor.
  - `readEON`, `readSDM`, and `readMB` now focus solely on link-specific logic and have been renamed to `readEONLinks`, `readSDMLinks`, and `readMBLinks` respectively.
- Eliminated redundant `#define MB 3`, introducing `constants.hpp` to centralize constants and avoid duplication.

## [0.8.0] - 2023-11-09
### ADDED

- Added support for Multiband: 
  - Added support for multiband for Link object.
  - Added Test cases in test_link.cpp for the corresponding methods of multiband Link object.
  - Added support for multiband for Network object.
  - Added Test cases in test_network.cpp for the corresponding methods of multiband Network object.
  - Added support for multiband for Bitrate object.
  - Added Test cases in test_bitrate.cpp for the corresponding methods of multiband Bitrate object.
- Added getters for BitRate class:
  - getBand(modulation,pos) Gets the char of the band in a given position (argument) inside the modulation.
  - getReachPerBand(modulation,pos) Gets the maximum reach distance in a given band (argument) inside the modulations reach vector.
  - getNumberOfSlotsPerBand(modulation,pos) Gets the number of slots in a given band (argument) inside the modulations slots vector. 
  - getNumberOfBands() Gets the number of bands available in the first modulation.
- Added getters for Link class:
  - getSlots(band) Get the size of the slots vector of the specified band of the link.
  - getBands() Get a vector of char values representing the bands from the link.
  - getSlot(pos,band) Get the state of a specific Slot inside the slots vector of the specified band. Active or inactive.
  - getNumberOfBands() Get the Number of bands attribute of the Link object.
- Added a JSON file with an example of multiband bitrate in the bitrate folder.
- Added a JSON file with an example of multiband network in the network folder.
- Added macros NUMBER_OF_BAND_MODULATION, GET_BAND, REQ_SLOTS_BDM, REQ_REACH_BDM, NUMBER_OF_BANDS, VECTOR_OF_BANDS, ALLOC_SLOTS_BDM and USE_UNALLOC_FUNCTION_BDM for BDM to Simulator class.
- Added macro REQ_POS_BANDS for BDM to Simulator class.
- Added BDM example.


## [0.7.1] - 2022-12-21
### ADDED

- Added macro NUMBER_OF_MODES for SDM to Simulator class.
- Added SDM example.
- Added version number to the library execution

### FIXED

- Fixed test.

## [0.7.0] - 2022-11-01
### ADDED

- Added support for SDM: 
  - Added support for multi-core and multi-mode for Link object
  - Added Test cases in test_link.cpp for the corresponding methods of multi-core/mode Link object.
  - Added support for multi-core and multi-mode for Network object.
  - Added Test cases in test_network.cpp for the corresponding methods of multi-core/mode Network object.
  - Added member class 'networkType' in order to know what type of network is being used and work accordingly.
  - Test cases in test_simulator.cpp for each network type creation and also for setters and getters of 'networkType' variable.
- Added getters for Simulator class:
  - getBitRates() gets the BitRates vector attributes of the simulator object.
  - getPaths() gets the paths for every pair of nodes.
  - Added tests.
- Added getters for BitRate class:
  - getNumberOfModulations() gets the number of modulations available in the BitRate object.
  - Added tests.
- Added macros NUMBER_OF_CORES, ALLOC_SLOTS_SDM and USE_UNALLOC_FUNCTION_SDM for SDM to Simulator class.

## [0.6.1] - 2022-10-17
### FIXED

- There was a bug that puts one blocked connection always. That was not significative in 1e6 events, but it was a bug.

## [0.6.0] - 2022-09-09
### ADDED

- A callback function after each disconnection.
- Added 5 new networks: ARPANET, EON, EUROcore, UKNet, USNet.

### FIXED

- Example 001 fixed. Reset slots in each route.
  
## [0.5.3] - 2022-05-19

### FIXED
 
- Changed the Agresti-Coull confidence interval's previously hardcoded solution only working for 95% confidence.

## [0.5.2] - 2022-03-21

### FIXED

- Changed the path of .json files (NSF Network and its routes) to the proper ones inside the second example's main file.

## [0.5.1] - 2022-03-15

### FIXED

- There was a typo in JSON network files, and the related class: lenght -> length
- Changing vector initialization error in examples. 

## [0.5.0] - 2021-12-22

### ADDED

- Confidence intervals: Wilson, Wald, and Agresti-Coull
- Added seed setters for the Src/Dst seeds' inside Simulator's class. 
- Test cases inside test_simulator.cpp for the corresponding Src/Dst seed setters.

### FIXED

- There was a typo in JSON network file, and the related class: lenght -> length
- Minor bugs

## [0.4.1] - 2021-07-28

### CHANGED

- The doxygen documentation of Link class was changed for a better compresion.
- Documentation of Simulator class:
  - Extended/changed comments on the class and methods description (.hpp).
- Documentation of Event class:
  - Extended/changed all comments on the class and methods description (.hpp).
- Documentation of Bit Rate class:
  - Extended/changed comments on the class and methods description (.hpp).
    
### ADDED

- Test case in test_simulator.cpp:
  - Added test case for testing UniformVariable class' non-void constructor and getNextValue method.
  - Was added the test for the setters methods for each variable.
  - Added constructor case.
- Test case in test_controller.cpp
    -Added test for checking no error throwing on Connection's addLink method.
- Test case in test_bitrate.cpp:
  - Added test for all of the BitRate's class getter methods.


## [0.4.0] - 2021-06-29

### ADDED

- New Feature: Three metrics for the Network, in network class.
  - Average Neighborhood
  - Normal Average Neighborhood
  - Nodal Variance

### CHANGED

- Documentation of Network/Controller class:
  - Extended/changed all comments on the class and methods description (.hpp).
  - Added comments on a method (isSlotUsed) to describe it clearer (.cpp).
  - Standarized some variables names: changed all similar variables names into one (i.e., from link and linkPos to linkPos), changed the variables in spanish into english and changed all variable names to camelCase (both .hpp and .cpp).

### DELETED

- nodeCounter variable.

## [0.3.0] - 2021-03-16

### ADDED

- Added Source/Destination as attributes of Link. Now Network is declared as a friend class within Link class.
- Added macros inside Simulator.hpp in order to obtain the Source/Destination of a link inside a route.
  - LINK_IN_ROUTE_SRC(route, link)
  - LINK_IN_ROUTE_DST(route, link)
- Macros added to the bitrate object in the algorithm implementation:
  - REQ*REACH(pos) Get the optical reach located in the \_pos* on the JSON file
  - REQ*MODULATION(pos) Get the modulation format located in the \_pos* on the JSON file
  - REQ_BITRATE_STR Get the bitrate value as string
  - REQ_BITRATE Get the bitrate value as double
- Adding getter to the simulation time with a method in simulator object
- Adding getter to the blocking probability with a method in simulator object

## [0.2.0] - 2021-03-06

### ADDED

- You can get the Bitrate value from a bitrate object
- Unit tests in BitRate class
- Exceptions in BitRate getters.
- One big header file added. If you want, no needs to compile the library. Just include the one big header file into your project.

### FIXED

- JSON BitRate method. Now the method get the values from json in an ordered way.

## [0.1.1] - 2021-03-02

### CHANGED

- Setting parameters of simulation must be done BEFORE the calling to init method. If you do it after, then an exception is thrown.

## [0.1.0] - 2021-02-26

### Added

- Elastic optical networks support
- Support for different bitrates through a JSON file
- Support for different networks through a JSON file
- Support for multiple routes through a JSON file
- Support the creation of your own allocation algorithm
- Customize connection arrive/departure ratios
- Support to create your own statistics
- Customize the number of connection arrives that you want to simulate

[0.1.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/releases/v0.1.0
[0.1.1]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.1.0...v0.1.1
[0.2.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.1.1...v0.2.0
[0.3.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.2.0...v0.3.0
[0.4.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.3.0...v0.4.0
[0.4.1]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.4.0...v0.4.1
[0.5.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.4.1...v0.5.0
[0.5.1]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.5.0...v0.5.1
[0.5.2]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.5.1...v0.5.2
[0.5.3]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.5.2...v0.5.3
[0.6.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.5.3...v0.6.0
[0.6.1]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.6.0...v0.6.1
[0.7.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.6.1...v0.7.0
[0.7.1]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.7.0...v0.7.1
[0.8.0]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.7.1...v0.8.0
[0.8.1]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.8.0...v0.8.1
[0.8.2]: https://gitlab.com/DaniloBorquez/flex-net-sim/-/compare/v0.8.1...v0.8.2