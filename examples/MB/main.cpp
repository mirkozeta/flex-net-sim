/*********************************************************************************
 * This is an allocation algorithm designed for Multi-Band (MB) networks.
 * It follows the First Fit allocation policy, selecting the available slots 
 * with the lowest possible index to serve the connection request.
 * 
 * The spectrum allocation considers multiple bands: C, L, S, and E.
 * These bands are prioritized in a fixed order to optimize resource allocation.
 * 
 * If the required slots are available, while maintaining spectrum contiguity 
 * and continuity constraints, the connection is established, returning 
 * 'ALLOCATED' to indicate success. Otherwise, it returns 'NOT_ALLOCATED' 
 * to indicate that the allocation attempt was unsuccessful.
 **********************************************************************************/

#include <fnsim/simulator.hpp>
// #include "./simulator.hpp"

// Global variables
std::vector<char> bandOrder;

BEGIN_ALLOC_FUNCTION(FirstFit) {

  // Control variables
  int routeLength;
  
  std::map<char, std::vector<bool>> slotState; // Slot state for each band
  int requiredSlots; // Number of required slots
  int totalSlots; // Total number of slots available in the route
  int index; // Index of the current slot
  
  for (size_t r = 0; r < NUMBER_OF_ROUTES; r++){ // RMLBSA: -> R (For each Route).

    // First we get the route length
    routeLength = 0;
    for (size_t l = 0; l < NUMBER_OF_LINKS(r); l++){
      routeLength += LINK_IN_ROUTE(r, l)->getLength();
    }
    for (size_t m = 0; m < NUMBER_OF_MODULATIONS; m++){ // RMLBSA: -> ML (For each Modulation Level).
      for (size_t b = 0; b < NUMBER_OF_BANDS(r, 0); b++){ // RMLBSA -> B (For each Band)

        // Check if the route length is greater than the reach of the modulation level
        if (REQ_REACH_MB(m, REQ_POS_BANDS(m)[bandOrder[b]]) < routeLength) continue;

        // Get the number of required slots given by the modulation level
        requiredSlots = REQ_SLOTS_MB(m, REQ_POS_BANDS(m)[bandOrder[b]]);

        // Get the representative vector of the slots in the route (r)
        // only if it has not been previously calculated (just for eficiency)
        if (slotState.find(bandOrder[b]) == slotState.end()){
          slotState[bandOrder[b]] = std::vector<bool>(LINK_IN_ROUTE(r, 0)->getSlots(bandOrder[b]), false);

          // Fill the vector with the slots of the route
          for (size_t l = 0; l < NUMBER_OF_LINKS(r); l++){
            for (size_t s = 0; s < LINK_IN_ROUTE(r, l)->getSlots(bandOrder[b]); s++){
              slotState[bandOrder[b]][s] = slotState[bandOrder[b]][s] | LINK_IN_ROUTE(r, l)->getSlot(s, bandOrder[b]);
            }
          }
        }

        // Given the representative vector of the slots in the route (r) for the band (b)
        // we look for the block of slots that satisfy the required slots
        totalSlots = 0;
        index = 0;

        for (size_t s = 0; s < slotState[bandOrder[b]].size(); s++){ // RMLBSA -> S (For each Slot)
          if (slotState[bandOrder[b]][s] == false){
            totalSlots++;
          } else {
            totalSlots = 0;
            index = s + 1;
          }
          if (totalSlots == requiredSlots){
            // Assign the slots to the connection
            for (size_t l = 0; l < NUMBER_OF_LINKS(r); l++){
              //std::cout << "Allocating slots in link " << LINK_IN_ROUTE_ID(r, l) << " band " << bandOrder[b] << " index " << index << " requiredSlots " << requiredSlots << std::endl;
              ALLOC_SLOTS_MB(LINK_IN_ROUTE_ID(r, l), bandOrder[b], index, requiredSlots);
            }
            return ALLOCATED;
          }
        }
      }
    }
  }
  return NOT_ALLOCATED;
}
END_ALLOC_FUNCTION

// For the moment, MB requires to define the unallocation function (can be empty)
BEGIN_UNALLOC_CALLBACK_FUNCTION{
}
END_UNALLOC_CALLBACK_FUNCTION

int main(int argc, char* argv[]) {

  // Define band order preferences
  bandOrder.assign({'C', 'L', 'S', 'E'});

  Simulator sim = Simulator(std::string("./NSFNet.json"),
                            std::string("./routes.json"),
                            std::string("./bitrates.json"),
                            MB);

  // Sim parameters
  USE_ALLOC_FUNCTION(FirstFit, sim);
  USE_UNALLOC_FUNCTION_MB(sim);

  sim.setGoalConnections(1e4);
  sim.setLambda(1500);
  sim.setMu(1);
  sim.init();
  sim.run();

  // Set precision for cout
  std::cout << std::fixed << std::setprecision(6);

  // Print results
  std::cout << "\nFinal blocking probability: " << sim.getBlockingProbability() << std::endl;

  return 0;
}
